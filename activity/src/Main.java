import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");
        int num = in.nextInt();

        int answer = 1;
        int counter = 1;

        try {

            if (num < 0) {
                System.out.println("Please enter a positive number");
            } else if (num == 0) {
                System.out.println("The factorial of 0 is 1");
            } else {
                // While loop
                while (counter <= num) {
                    answer *= counter;
                    counter++;
                }

                System.out.println("The factorial of " + num + " is " + answer);


                // For loop
                answer = 1;
                for (int i = 1; i <= num; i++) {
                    answer *= i;
                }

                System.out.println("The factorial of " + num + " is " + answer);
            }

        } catch (Exception e) {
            System.out.println("Invalid input");
            e.printStackTrace();

        } finally {
            in.close();
        }
    }
}